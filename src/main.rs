#![no_main]
#![no_std]

use core::{cell::RefCell, ops::DerefMut, panic::PanicInfo};

/// This function is called on panic.
#[inline(never)]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    rprintln!("{}", info);
    loop {} // You might need a compiler fence in here.
}

// basic entry and debugging
use cortex_m_rt::entry;
use rtt_target::{rprintln, rtt_init_print};

// interrupt handling
use cortex_m::interrupt::{free, Mutex};
use stm32f4xx_hal::{
    gpio::{gpioc::PC7, Edge, Input},
    interrupt, pac,
    prelude::*,
};

static BUTTON: Mutex<RefCell<Option<PC7<Input>>>> = Mutex::new(RefCell::new(None));

#[entry]
fn main() -> ! {
    rtt_init_print!();

    /* Get access to device and core peripherals */
    let dp = pac::Peripherals::take().unwrap();
    let cp = cortex_m::Peripherals::take().unwrap();

    /* Get access to RCC and GPIOA */
    let rcc = dp.RCC.constrain();
    let mut syscfg = dp.SYSCFG.constrain();
    let mut exti = dp.EXTI;
    let gpioa = dp.GPIOA.split();
    let gpioc = dp.GPIOC.split();

    /* Set up LED pin */
    let mut led = gpioa.pa5.into_push_pull_output();

    /* Set up button pin */
    // Create a button input with an interrupt
    let mut board_btn = gpioc.pc7.into_pull_down_input();
    board_btn.make_interrupt_source(&mut syscfg);
    board_btn.enable_interrupt(&mut exti);
    board_btn.trigger_on_edge(&mut exti, Edge::RisingFalling);

    free(|cs| {
        BUTTON.borrow(cs).replace(Some(board_btn));
    });

    // Enable interrupts
    pac::NVIC::unpend(pac::Interrupt::EXTI15_10);
    unsafe {
        pac::NVIC::unmask(pac::Interrupt::EXTI15_10);
    };

    /* Set up sysclk and freeze it */
    let clocks = rcc.cfgr.sysclk(84.MHz()).freeze();

    /* Set up systick delay */
    let mut delay = cp.SYST.delay(&clocks);

    loop {
        /* Light show */
        led.set_high();
        rprintln!("LED on");
        delay.delay_ms(1_000_u32);
        led.set_low();
        rprintln!("LED off");
        delay.delay_ms(1_000_u32);
    }
}

#[interrupt]
fn EXTI15_10() {
    free(|cs| {
        let mut btn_ref = BUTTON.borrow(cs).borrow_mut();
        if let Some(ref mut btn) = btn_ref.deref_mut() {
            // We cheat and don't bother checking _which_ exact interrupt line fired - there's only
            // ever going to be one in this example.
            btn.clear_interrupt_pending_bit();

            rprintln!("Got an interrupt");
        }
    });
}
