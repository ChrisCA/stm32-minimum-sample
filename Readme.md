# Setup and installation

1) cargo install cargo-flash
2) cargo install cargo-embed
3) Checkout probe.rs
4) Install STLink driver via ST website
5) Create Embed.toml and add https://github.com/probe-rs/cargo-embed/blob/master/src/config/default.toml and then enable RTT debugging
6) Deactivate "all-target" check on save for rust-analyzer
7) Add target to config.toml
8) Add linker script for memory.x to config.toml
9) Install binutils via cargo install cargo-binutils which is neccessary to check e.g. with cargo readobj --target thumbv7em-none-eabihf --bin <nameofyourbinary> -- --file-header
10) Add LLVM preview rustup component add llvm-tools-preview
11) Add target for board, e.g. rustup target add thumbv7em-none-eabihf
